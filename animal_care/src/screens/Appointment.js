import React from 'react';
import { withNavigation } from 'react-navigation';
import { Platform, StatusBar, StyleSheet, View,Text,ScrollView,Dimensions,FlatList } from 'react-native';
import { DefaultTheme, Provider as PaperProvider } from 'react-native-paper';
import { Appbar } from 'react-native-paper';
import { TextInput,Title,Button,Surface } from 'react-native-paper';
import { Avatar,Divider,Drawer } from 'react-native-paper';
import { List, Checkbox } from 'react-native-paper';
import {Link} from './global';

const doctorsf = [];
class App extends React.Component {
  static navigationOptions = {
    header: null,
  };
  state = {
    isLoadingComplete: false,
    text:'',
    user:'',
    active:'first',
    expanded: true,
    data:[],
    doctors:[],
    selectedOrg:null,
    selectedDoctor:null,
    labelDoctor:"Choose Doctor",
    labelOrg:"Choose Organization",
    doctorsf:[],
    selectedTime:"",
    timeLabel:"Select Preferred Time"
  };
  _handlePress = () =>
  this.setState({
    expanded: !this.state.expanded
  });
  changeDoctor=(id,name)=>{
    
    this.setState({
      selectedDoctor:id,
      labelDoctor:name
    })
   // alert("Changed , Doctor Name :" + name + "id :" + id)
  }

 
  changeOrg=(id,name)=>
  {
    doctorsf.length=0;
    this.setState({
      selectedOrg:id,
      labelOrg:name,
    })
    const select = this.state.selectedOrg;
    if(this.state.selectedOrg!=null)
    {
        this.state.doctors.map(function(data,id){
          //alert("Selected Organization :" + select+ "Doctor Organization " +data.Organization)

          if(data.Organization === select)
          {
            doctorsf.push(data)
          }
        })
    }
    //alert("Changed , Organization Name :" + name + "id :" + id)
  }
  componentDidMount()
  {
    
   alert("Getting Organization");
    fetch(`${Link}/organization/all`, {
      method: 'GET',
     
      
    })
    
      .then((response) => response.json())
      .then((responseJson) => {
          console.log(responseJson, 'res JSON');
          this.setState({data : responseJson })
          //alert(this.state.data[0].company_name)
      })
      .catch((error) => {
          console.error(error);
      });

      fetch(`${Link}/organization/allDoctor`, {
        method: 'GET',
       
        
      })
      
        .then((response) => response.json())
        .then((responseJson) => {
            console.log(responseJson, 'res JSON');
            this.setState({doctors : responseJson })
            //alert(this.state.doctors[0].description)
        })
        .catch((error) => {
            console.error(error);
        });
  }
  submit=(stat)=>
  {
    alert("Submitting");
    //alert("Time : " + this.state.timeLabel + " , Org : " + this.state.selectedOrg + " ,Doctor : " + this.state.selectedDoctor);
    
    this.props.navigation.navigate('Appointment2',{doctorName:this.state.labelDoctor,doctorID:this.state.selectedDoctor,orgID:this.state.selectedOrg,time:this.state.timeLabel,orgName:this.state.labelOrg})
  }
  render() {
    const { active } = this.state;
    const screenHeight = Dimensions.get('window').height;
      return (
        <PaperProvider theme={theme} style={{width:'80%',height:screenHeight}}>
        <Appbar.Header>
        <Avatar.Icon size={50} style={{marginRight:2}} icon="beach-access" />
        <Appbar.Content
          title="Save Tails" style={{marginLeft:2}}
        />
       
      </Appbar.Header>
        
      
        <Surface style={styles.surface}>
        <Title style={{color:"#9134af"}}>Book An Appointment (Step 1/2) </Title>
        <ScrollView 
    behaviour = "height"
    keyboardVerticalOffset = {64}
    style= {{ marginTop: '5%',width:"100%",height:"100%",padding:5}}
  >
        
        <List.Section title="Step 1">
        <List.Accordion
          title={this.state.labelOrg}
          left={props => <List.Icon {...props} icon="chevron-right" />}
        >
        <FlatList
         data={this.state.data}
        renderItem={({item}) => 
          <List.Item title={item.company_name} onPress={()=>this.changeOrg(item._id,item.company_name)} />
          }/>
        </List.Accordion>

        <List.Accordion
          title={this.state.labelDoctor}
          left={props => <List.Icon {...props} icon="chevron-right" />}
        >
          <FlatList
         data={doctorsf}
        renderItem={({item}) => 
          <List.Item title={item.name} onPress={()=>this.changeDoctor(item._id,item.name)} />
          }/>
          </List.Accordion>
          <List.Accordion
          title={this.state.timeLabel}
          left={props => <List.Icon {...props} icon="chevron-right" />}
        >
          
          <List.Item title="8 AM - 9 AM" onPress={()=>this.setState({timeLabel:"8 AM - 9 AM"})} />
          <List.Item title="9 AM - 10 AM" onPress={()=>this.setState({timeLabel:"9 AM - 10 AM"})} />
          <List.Item title="10 AM - 11 AM" onPress={()=>this.setState({timeLabel:"10 AM - 11 AM"})} />
          <List.Item title="11 AM - 12 PM" onPress={()=>this.setState({timeLabel:"11 AM - 12 PM"})} />
          <List.Item title="12 PM - 1 PM" onPress={()=>this.setState({timeLabel:"12 PM - 1 PM"})} />
          <List.Item title="12 PM - 1 PM" onPress={()=>this.setState({timeLabel:"12 PM - 1 PM"})} />
          <List.Item title="1 PM - 2 PM" onPress={()=>this.setState({timeLabel:"1 PM - 2 PM"})} />
          
          
        
        </List.Accordion>

        
      </List.Section>
     


  <Button icon="add-circle-outline" mode="contained" style={styles.button} color="#9134af" onPress={() => this.submit(1)}>
    Next
  </Button>

  <Button icon="backspace" mode="contained" style={styles.button} color="#9134af" onPress={() => this.props.navigation.navigate('Home')}>
      
    Cancel
  </Button>
 <View style={{padding:50}}></View>

  </ScrollView>
  </Surface>
        </PaperProvider>
      );
    }
  }
  export default withNavigation(App);
  const theme = {
    ...DefaultTheme,
    colors: {
      ...DefaultTheme.colors,
      primary: '#9134af',
      accent: '#ff5252',
    },
  };
  const styles = StyleSheet.create({
    bottom: {
      position: 'absolute',
      left: 0,
      right: 0,
      bottom: 0,
    },
    text:{
      width:'95%',
      borderRadius:0,
      backgroundColor:"white",
      marginBottom:10
    }
    ,
    button:{
      color:'white',
      marginTop:10,
      width:'95%'
    },
    surface: {
      padding: 8,
      marginTop:10,
      paddingBottom:25,
      //height: 80,
      width: '100%',
      alignItems: 'center',
      justifyContent: 'center',
      elevation: 0,
    },
  });
