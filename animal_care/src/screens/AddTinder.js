import React from 'react';
import { Platform, StatusBar, StyleSheet, View,Text,ScrollView,Dimensions,Image } from 'react-native';
import { DefaultTheme, Provider as PaperProvider } from 'react-native-paper';
import { Appbar } from 'react-native-paper';
import { TextInput,Title,Button,Surface } from 'react-native-paper';
import { Avatar,Divider,Drawer } from 'react-native-paper';
import {Link} from './global';
import { List, Checkbox } from 'react-native-paper';
var ImagePicker = require('react-native-image-picker');
export default class App extends React.Component {
  static navigationOptions = {
    header: null,
  };
  state = {
    filePath: {},
    isLoadingComplete: false,
    text:'',
    user:'',
    active:'first',
    name:'',
    address:'',
    description:'',
    price:'',
    pet_sex:'',
    pet_age:'',
    conatct:'',
    data : new FormData(),

    expanded: true
  };
  _handlePress = () =>
  this.setState({
    expanded: !this.state.expanded
  });

 
  trySignUp  = (st) =>{
    alert("Submitting")
    this.state.data.append('conatct',this.state.conatct)
    this.state.data.append('address',this.state.address)
        this.state.data.append('pet_name',this.state.name)
        this.state.data.append('pet_description',this.state.description)
        this.state.data.append('price',this.state.price)
        this.state.data.append('id','5cd753168b9ed41dc7fe917e')
    console.log("Create Account Function Called");
    fetch(`${Link}/user/addPet`, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'multipart/form-data',
      },
      body: this.state.data

      //data : this.state.data,
      
    })
      .then((response) => response.json())
      .then((responseJson) => {
          console.log(responseJson, 'res JSON');
          //alert(responseJson.message);
          console.log(this.state.user);
          console.log(this.state.password);
          
          if(responseJson.message=="done")
          {
            alert("Post Submitted Successfully !");
          }
          else{
            alert("Failed")
          }

          alert("Request Submitted Successfully");
            
          
      })
      .catch((error) => {
          console.error(error);
          alert(error);
      });
      }




  chooseFile = () => {
    var options = {
      title: 'Select Image',
      customButtons: [
        { name: 'customOptionKey', title: 'Choose Photo from Custom Option' },
      ],
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };
    ImagePicker.showImagePicker(options, response => {
      console.log('Response = ', response);
 
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
        alert(response.customButton);
      } else {
        let source = response;
        // You can also display the image using data:
        // let source = { uri: 'data:image/jpeg;base64,' + response.data };
        
        
        
       

        this.state.data.append('TinderImage', {
         uri : response.uri,
         type: response.type,
         name: response.fileName,
        });
        
        this.setState({
          filePath: source,
        });
      }
    });
  };
  render() {
    const { active } = this.state;
    const screenHeight = Dimensions.get('window').height;
      return (
        <PaperProvider theme={theme} style={{width:'80%',height:screenHeight}}>
        <Appbar.Header>
        <Avatar.Icon size={50} style={{marginRight:2}} icon="beach-access" />
        <Appbar.Content
          title="Save Tails" style={{marginLeft:2}}
        />
       
      </Appbar.Header>
        
      
        <Surface style={styles.surface}>
        <Title style={{color:"#9134af"}}>Add Tinder Post </Title>
        <ScrollView 
    behaviour = "height"
    keyboardVerticalOffset = {64}
    style= {{ marginTop: '5%',width:"100%",height:"100%",padding:5}}
  >

<Button icon="add-circle-outline" mode="contained" style={styles.button} color="#9134af" onPress={this.chooseFile.bind(this)}>
    
    Attach Picture
  </Button>
  <Image
              source={{
                uri: 'data:image/jpeg;base64,' + this.state.filePath.data,
              }}
              style={{ width: 100, height: 100 }}
            />
        
       
     
      <TextInput style={styles.text}
        label='Pet Name'
        Type="outlined"
        value={this.state.name}
        onChangeText={name => this.setState({ name })}
      />
      <TextInput style={styles.text}
        label='Pet sex'
        Type="outlined"
        value={this.state.pet_sex}
        onChangeText={pet_sex => this.setState({ pet_sex })}
      />
      <TextInput style={styles.text}
        label='Pet Age'
        Type="outlined"
        value={this.state.pet_age}
        onChangeText={pet_age => this.setState({ pet_age })}
      />
      <TextInput style={styles.text}
        label='Contact #'
        Type="outlined"
        value={this.state.contact}
        onChangeText={contact => this.setState({ contact })}
      />

<TextInput style={styles.text}
        label='Address'
        Type="outlined"
        value={this.state.address}
        onChangeText={address => this.setState({ address })}
      />

  <Button icon="add-circle-outline" mode="contained" style={styles.button} color="#9134af" onPress={()=>this.trySignUp(1)}>
    Add Tinder Post 
  </Button>

  <Button icon="backspace" mode="contained" style={styles.button} color="#9134af" onPress={() => this.props.navigation.navigate('Home')}>
      
    Cancel
  </Button>
 <View style={{padding:50}}></View>

  </ScrollView>
  </Surface>
        </PaperProvider>
      );
    }
  }
  const theme = {
    ...DefaultTheme,
    colors: {
      ...DefaultTheme.colors,
      primary: '#9134af',
      accent: '#ff5252',
    },
  };
  const styles = StyleSheet.create({
    bottom: {
      position: 'absolute',
      left: 0,
      right: 0,
      bottom: 0,
    },
    text:{
      width:'95%',
      borderRadius:0,
      backgroundColor:"white",
      marginBottom:10
    }
    ,
    button:{
      color:'white',
      marginTop:10,
      width:'95%'
    },
    surface: {
      padding: 8,
      marginTop:10,
      paddingBottom:25,
      //height: 80,
      width: '100%',
      alignItems: 'center',
      justifyContent: 'center',
      elevation: 0,
    },
  });
