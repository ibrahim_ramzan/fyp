import React from 'react';
import { Platform, StatusBar, StyleSheet,Alert, View,Text,ScrollView,Dimensions } from 'react-native';
import { DefaultTheme, Provider as PaperProvider } from 'react-native-paper';
import { Appbar } from 'react-native-paper';
import { TextInput,Title,Button,Surface } from 'react-native-paper';
import { Avatar,Divider,Drawer } from 'react-native-paper';
import {Link} from './global';
export default class App extends React.Component {
  static navigationOptions = {
    header: null,
  };
  state = {
    isLoadingComplete: false,
    comnpany_name : '',
    email : '',
    city : '',
    phoneNo : '',
    address : '',
    bio:'',
    timing : '',
    ISO : '',
    website : '',
    text:'',
    user:'',
    reg:'',
    password:'',
    active:'first'
  };

  trySignUp  = () =>{
    console.log("Create Account Function Called");
    fetch(`${Link}/organization/addDoctor`, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        company_name : this.state.comnpany_name,
        phone_no: this.state.phoneNo,
        address: this.state.address,
        email:this.state.email,
        city: this.state.city,
        register_no: this.state.reg,
        webiste: this.state.website,
        email: this.state.email,
        bio: this.state.bio,
        password: this.state.password,
      }),
    })
      .then((response) => response.json())
      .then((responseJson) => {
          console.log(responseJson, 'res JSON');
          //alert(responseJson.message);
         
          if(responseJson.message == "Organization created")
          {
            alert("Account Created Successfully");
            this.props.navigation.navigate('SignIn');
          }
          else{
            alert(responseJson.message);
          }
      })
      .catch((error) => {
          console.error(error);
      });
      }

  render() {

    
    const { active } = this.state;
    const screenHeight = Dimensions.get('window').height;
      return (
        <PaperProvider theme={theme} style={{width:'80%',height:screenHeight}}>
        <Appbar.Header>
        <Avatar.Icon size={50} style={{marginRight:2}} icon="beach-access" />
        <Appbar.Content
          title="Save Tails" style={{marginLeft:2}}
        />
       
      </Appbar.Header>
        
      
        <Surface style={styles.surface}>
        <Title style={{color:"#9134af"}}>Create New Account </Title>
        <ScrollView 
    behaviour = "height"
    keyboardVerticalOffset = {64}
    style= {{ marginTop: '5%',width:"100%",height:"100%",padding:5}}
  >
        
        <TextInput style={styles.text}
        label='Company Name'
        Type="outlined"
        value={this.state.comnpany_name}
        onChangeText={comnpany_name => this.setState({ comnpany_name })}
      />
        <TextInput style={styles.text}
        label='Email Address'
        Type="outlined"
        value={this.state.email}
        onChangeText={email => this.setState({ email })}
      />
      <TextInput style={styles.text}
        label='Password'
        Type="outlined"
        secureTextEntry={true}
        value={this.state.password}
        onChangeText={password => this.setState({ password })}
      />
     <TextInput style={styles.text}
        label='City'
        Type="outlined"
        value={this.state.city}
        onChangeText={city => this.setState({ city })}
      />
      <TextInput style={styles.text}
        label='Phone No.'
        Type="outlined"
        value={this.state.phoneNo}
        onChangeText={phoneNo => this.setState({ phoneNo })}
      />
       <TextInput style={styles.text}
        label='Address'
        Type="outlined"
        value={this.state.address}
        onChangeText={address => this.setState({ address })}
      />
       
       <TextInput style={styles.text}
        label='Reg #'
        Type="outlined"
        value={this.state.reg}
        onChangeText={reg => this.setState({ reg })}
      />
       <TextInput style={styles.text}
        label='Website (Optional)'
        Type="outlined"
        value={this.state.website}
        onChangeText={website => this.setState({ website })}
      />
      <TextInput style={styles.text}
        label='BIO'
        Type="outlined"
        value={this.state.bio}
        onChangeText={bio => this.setState({ bio })}
      />
     


  <Button icon="add-circle-outline" mode="contained" style={styles.button} color="#9134af" onPress={this.trySignUp}>
      
    Create Account
  </Button>

  <Button icon="backspace" mode="contained" style={styles.button} color="#9134af" onPress={() => this.props.navigation.navigate('SignIn')}>
      
    Cancel
  </Button>
 <View style={{padding:50}}></View>

  </ScrollView>
  </Surface>
        </PaperProvider>
      );
    }
  }
  const theme = {
    ...DefaultTheme,
    colors: {
      ...DefaultTheme.colors,
      primary: '#9134af',
      accent: '#ff5252',
    },
  };
  const styles = StyleSheet.create({
    bottom: {
      position: 'absolute',
      left: 0,
      right: 0,
      bottom: 0,
    },
    text:{
      width:'95%',
      borderRadius:0,
      backgroundColor:"white",
      marginBottom:10
    }
    ,
    button:{
      color:'white',
      marginTop:10,
      width:'95%'
    },
    surface: {
      padding: 8,
      marginTop:10,
      paddingBottom:25,
      //height: 80,
      width: '100%',
      alignItems: 'center',
      justifyContent: 'center',
      elevation: 0,
    },
  });
