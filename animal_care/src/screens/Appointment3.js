import React from 'react';
import { Platform, StatusBar, StyleSheet, View,Text,ScrollView,Dimensions } from 'react-native';
import { DefaultTheme, Provider as PaperProvider } from 'react-native-paper';
import { Appbar } from 'react-native-paper';
import { TextInput,Title,Button,Surface } from 'react-native-paper';
import { Avatar,Divider,Drawer } from 'react-native-paper';
import { List, Checkbox } from 'react-native-paper';
export default class App extends React.Component {
  static navigationOptions = {
    header: null,
  };
  state = {
    isLoadingComplete: false,
    text:'',
    user:'',
    active:'first',
    expanded: true
  };
  _handlePress = () =>
  this.setState({
    expanded: !this.state.expanded
  });
  render() {
    const { active } = this.state;
    const screenHeight = Dimensions.get('window').height;
      return (
        <PaperProvider theme={theme} style={{width:'80%',height:screenHeight}}>
        <Appbar.Header>
        <Avatar.Icon size={50} style={{marginRight:2}} icon="beach-access" />
        <Appbar.Content
          title="Save Tails" style={{marginLeft:2}}
        />
       
      </Appbar.Header>
        
      
        <Surface style={styles.surface}>
        <Title style={{color:"#b33939"}}>Book An Appointment (Step 3/3) </Title>
        <ScrollView 
    behaviour = "height"
    keyboardVerticalOffset = {64}
    style= {{ marginTop: '5%',width:"100%",height:"100%",padding:5}}
  >
        
        <List.Section title="Accordions">
        <List.Accordion
          title="Your Chosen Practice  "
          left={props => <List.Icon {...props} icon="chevron-right" />}
        >
          <List.Item title="Option 1" />
          <List.Item title="Option 2" />
        </List.Accordion>

        <List.Accordion
          title="Select Vetnary  "
          left={props => <List.Icon {...props} icon="chevron-right" />}
        >
          <List.Item title="Option 1" />
          <List.Item title="Option 2" />
          </List.Accordion>
          <List.Accordion
          title="Select Prefered Time  "
          left={props => <List.Icon {...props} icon="chevron-right" />}
        >
          <List.Item title="Option 1" />
          <List.Item title="Option 2" />
        
        </List.Accordion>

        
      </List.Section>
     


  <Button icon="add-circle-outline" mode="contained" style={styles.button} color="#b33939" onPress={() => console.log('Login Pressed')}>
    Next
  </Button>

  <Button icon="backspace" mode="contained" style={styles.button} color="#b33939" onPress={() => this.props.navigation.navigate('Home')}>
      
    Cancel
  </Button>
 <View style={{padding:50}}></View>

  </ScrollView>
  </Surface>
        </PaperProvider>
      );
    }
  }
  const theme = {
    ...DefaultTheme,
    colors: {
      ...DefaultTheme.colors,
      primary: '#b33939',
      accent: '#ff5252',
    },
  };
  const styles = StyleSheet.create({
    bottom: {
      position: 'absolute',
      left: 0,
      right: 0,
      bottom: 0,
    },
    text:{
      width:'95%',
      borderRadius:0,
      backgroundColor:"white",
      marginBottom:10
    }
    ,
    button:{
      color:'white',
      marginTop:10,
      width:'95%'
    },
    surface: {
      padding: 8,
      marginTop:10,
      paddingBottom:25,
      //height: 80,
      width: '100%',
      alignItems: 'center',
      justifyContent: 'center',
      elevation: 0,
    },
  });
