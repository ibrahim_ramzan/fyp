import React from 'react';
import { Platform, StatusBar, StyleSheet, View,Text,ScrollView,Dimensions } from 'react-native';
import { DefaultTheme, Provider as PaperProvider } from 'react-native-paper';
import { Appbar } from 'react-native-paper';
import { TextInput,Title,Button,Surface } from 'react-native-paper';
import { Avatar,Divider,Drawer } from 'react-native-paper';
import { List, Checkbox } from 'react-native-paper';
import {Link} from './global';
export default class App extends React.Component {
  static navigationOptions = {
    header: null,
  };
  state = {
    isLoadingComplete: false,
    text:'',
    user:'',
    active:'first',
    expanded: true,
    age:0,
    gender:"Male",
    name:"",
    reason:"",
    species:""
  };
  _handlePress = () =>
  this.setState({
    expanded: !this.state.expanded
  });
  bookAppointment = (st) =>{
    //alert("LSubmit Function Called");
    fetch(`${Link}/user/bookAppointment`, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        organization_name: this.props.navigation.getParam('orgName',""),
        organization_id:this.props.navigation.getParam('orgID',0),
        doctor_name: this.props.navigation.getParam('doctorName',""),
        doctor_id: this.props.navigation.getParam('doctorID',0),
        time: this.props.navigation.getParam('time',""),
        pet_name: this.state.name,
        pet_sex:this.state.gender,
        pet_age:this.state.age,
        pet_species:this.state.species,
        appointment_reason:this.state.reason,
        user_id:"5ce5f2a895c51508d0deeb47"

      }),
    })
    
      .then((response) => response.json())
      .then((responseJson) => {
        //alert(responseJson.message)
        if(responseJson.message=="done")
        {
          alert("Appointment Booked  !");
        }
        else{
          alert(responseJson.message)
        }
      })
      .catch((error) => {
          console.error(error);
      });
      }
  render() {
    const { active } = this.state;
    const screenHeight = Dimensions.get('window').height;
      return (
        <PaperProvider theme={theme} style={{width:'80%',height:screenHeight}}>
        <Appbar.Header>
        <Avatar.Icon size={50} style={{marginRight:2}} icon="beach-access" />
        <Appbar.Content
          title="Save Tails" style={{marginLeft:2}}
        />
       
      </Appbar.Header>
        
      
        <Surface style={styles.surface}>
        <Title style={{color:"#9134af"}}>Book An Appointment (Step 2/2) </Title>
        <ScrollView 
    behaviour = "height"
    keyboardVerticalOffset = {64}
    style= {{ marginTop: '5%',width:"100%",height:"100%",padding:5}}
  >
        <TextInput style={styles.text}
        label='Pet Name'
        Type="outlined"
        value={this.state.name}
        onChangeText={name => this.setState({ name })}
      />


        <List.Section title="Step 2">
        <List.Accordion
          title="Pet Sex  "
          left={props => <List.Icon {...props} icon="chevron-right" />}
        >
          <List.Item title="Male" onPress={()=>this.setState({gender:"Male"})}/>
          <List.Item title="Female" onPress={()=>this.setState({gender:"Female"})} />
        </List.Accordion>

        <List.Accordion
          title="Pet Age  "
          left={props => <List.Icon {...props} icon="chevron-right" />}
        >
          <List.Item title="1" onPress={()=>this.setState({age:1})} />
          <List.Item title="2" onPress={()=>this.setState({age:2})}/>
          <List.Item title="3" onPress={()=>this.setState({age:3})}/>
          <List.Item title="4" onPress={()=>this.setState({age:4})}/>
          <List.Item title="5" onPress={()=>this.setState({age:5})}/>
          <List.Item title="6" onPress={()=>this.setState({age:6})} />
          <List.Item title="7" onPress={()=>this.setState({age:7})} />
          <List.Item title="8" onPress={()=>this.setState({age:8})} />
          <List.Item title="9" onPress={()=>this.setState({age:9})} />
          </List.Accordion>
          <List.Accordion
          title="Pet Species  "
          left={props => <List.Icon {...props} icon="chevron-right" />}
        >
          <List.Item title="Option 1" onPress={()=>this.setState({species:"Option 1"})}/>
          <List.Item title="Option 2" onPress={()=>this.setState({species:"Option 2"})} />
        
        </List.Accordion>

        <List.Accordion
          title="Reason For Appointment  "
          left={props => <List.Icon {...props} icon="chevron-right" />}
        >
          <List.Item title="Vaccination" onPress={()=>this.setState({reason:"Vaccination"})} />
          <List.Item title="Dental Check" onPress={()=>this.setState({reason:"Dental Check"})} />
          <List.Item title="Vet Consultation" onPress={()=>this.setState({reason:"Vet Consultation"})} />
          <List.Item title="Other" onPress={()=>this.setState({reason:"Other"})} />

        
        </List.Accordion>

        
      </List.Section>
      <TextInput style={styles.text}
        label='If Other , Please Specify'
        Type="outlined"
        value={this.state.reason}
        onChangeText={reason => this.setState({ reason })}
      />


  <Button icon="add-circle-outline" mode="contained" style={styles.button} color="#9134af" onPress={() => this.bookAppointment(1)}>
    Book Appointment
  </Button>

  <Button icon="backspace" mode="contained" style={styles.button} color="#9134af" onPress={() => this.props.navigation.navigate('Home')}>
      
    Cancel
  </Button>
 <View style={{padding:50}}></View>

  </ScrollView>
  </Surface>
        </PaperProvider>
      );
    }
  }
  const theme = {
    ...DefaultTheme,
    colors: {
      ...DefaultTheme.colors,
      primary: '#9134af',
      accent: '#ff5252',
    },
  };
  const styles = StyleSheet.create({
    bottom: {
      position: 'absolute',
      left: 0,
      right: 0,
      bottom: 0,
    },
    text:{
      width:'95%',
      borderRadius:0,
      backgroundColor:"white",
      marginBottom:10
    }
    ,
    button:{
      color:'white',
      marginTop:10,
      width:'95%'
    },
    surface: {
      padding: 8,
      marginTop:10,
      paddingBottom:25,
      //height: 80,
      width: '100%',
      alignItems: 'center',
      justifyContent: 'center',
      elevation: 0,
    },
  });
