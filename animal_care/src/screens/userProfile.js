import React from 'react';
import { Platform, StatusBar, StyleSheet, View,Text,ScrollView,Dimensions } from 'react-native';
import { DefaultTheme, Provider as PaperProvider } from 'react-native-paper';
import { Appbar } from 'react-native-paper';
import { TextInput,Title,Button,Surface } from 'react-native-paper';
import { Avatar,Divider,Drawer } from 'react-native-paper';
import { List, Checkbox } from 'react-native-paper';
export default class App extends React.Component {
  static navigationOptions = {
    header: null,
  };
  state = {
    isLoadingComplete: false,
    text:'',
    name:'Testing and',
    email:'testing@email.com',
    contact:'+123-456-789',
    active:'first',
    expanded: true
  };
  _handlePress = () =>
  this.setState({
    expanded: !this.state.expanded
  });
  render() {
    const { active } = this.state;
    const screenHeight = Dimensions.get('window').height;
      return (
        <PaperProvider theme={theme} style={{width:'80%',height:screenHeight}}>
        <Appbar.Header>
        <Avatar.Icon size={50} style={{marginRight:2}} icon="person" />
        <Appbar.Content
          title="Profile" style={{marginLeft:2}}
        />
       
      </Appbar.Header>
        
      
        <Surface style={styles.surface}>
        
        
        <Avatar.Image size={134} source={require('../assets/place.jpg')}  />
        <TextInput style={styles.text}
        label='Name'
        Type="outlined"
        value={this.state.name}
        onChangeText={name => this.setState({ name })}
      />
      <TextInput style={styles.text}
        label='Email'
        Type="outlined"
        value={this.state.email}
        onChangeText={email => this.setState({ email })}
      />
    
      <TextInput style={styles.text}
        label='Contact Number'
        Type="outlined"
        value={this.state.contact}
        onChangeText={contact => this.setState({ contact })}
      />
     


  <Button icon="add-circle-outline" mode="contained" style={styles.button} color="#9134af" onPress={() => this.props.navigation.navigate('Home')}>
    Update Profile
  </Button>

  <Button icon="backspace" mode="contained" style={styles.button} color="#9134af" onPress={() => this.props.navigation.navigate('Home')}>
      
    Back
  </Button>
 <View style={{padding:50}}></View>

  
  </Surface>
        </PaperProvider>
      );
    }
  }
  const theme = {
    ...DefaultTheme,
    colors: {
      ...DefaultTheme.colors,
      primary: '#9134af',
      accent: '#ff5252',
    },
  };
  const styles = StyleSheet.create({
    bottom: {
      position: 'absolute',
      left: 0,
      right: 0,
      bottom: 0,
    },
    text:{
      width:'95%',
      borderRadius:0,
      backgroundColor:"white",
      marginBottom:10
    }
    ,
    button:{
      color:'white',
      marginTop:10,
      width:'95%'
    },
    surface: {
      padding: 8,
      marginTop:10,
      paddingBottom:25,
      //height: 80,
      width: '100%',
      alignItems: 'center',
      justifyContent: 'center',
      elevation: 0,
    },
  });
