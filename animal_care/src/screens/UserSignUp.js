import React from 'react';
import { Platform, StatusBar, StyleSheet, View,Text } from 'react-native';
import { DefaultTheme, Provider as PaperProvider } from 'react-native-paper';
import { Appbar } from 'react-native-paper';
import { TextInput,Title,Button,Surface } from 'react-native-paper';
import { Avatar,Divider,Drawer } from 'react-native-paper';
import {Link} from './global';
export default class App extends React.Component {
  static navigationOptions = {
    header: null,
  };
  constructor(props) {
    super(props);
  this.state = {
    isLoadingComplete: false,
    password:'',
    user:'',
    email:'',
    active:'first',
  };
  }
  trySignUp  = () =>{
    console.log("Create Account Function Called");
    fetch(`${Link}/user/signup`, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        email: this.state.email,
        password: this.state.password,
        userName:this.state.user
      }),
    })
      .then((response) => response.json())
      .then((responseJson) => {
          console.log(responseJson, 'res JSON');
          //alert(responseJson.message);
          
          if(responseJson.message == "User created")
          {
            alert("Account Created Successfully");
            this.props.navigation.navigate('SignIn');
          }
          else{
            alert(responseJson.message);
          }
      })
      .catch((error) => {
          console.error(error);
      });
      }
  render() {
    const { active } = this.state;
      return (
        <PaperProvider theme={theme} style={{width:'80%'}}>
        <Appbar.Header>
        <Avatar.Icon size={50} style={{marginRight:2}} icon="beach-access" />
        <Appbar.Content
          title="Save Tails" style={{marginLeft:2}}
        />
      </Appbar.Header>
        <Surface style={styles.surface}>
        <Title style={{color:"#9134af"}}>Create New Account </Title>
        <TextInput style={styles.text}
        label='Email Address'
        Type="outlined"
        value={this.state.email}
        onChangeText={email => this.setState({ email })}
      />
      <TextInput style={styles.text}
        label='Username'
        Type="outlined"
        value={this.state.user}
        onChangeText={user => this.setState({ user })}
      />
      <TextInput style={styles.text}
        label='Password'
        Type="outlined"
        secureTextEntry={true}
        value={this.state.password}
        onChangeText={password => this.setState({ password })}
      />
      
  <Button icon="add-circle-outline" mode="contained" style={styles.button} color="#9134af" onPress={this.trySignUp}>
    Create Account
  </Button>
  <Button icon="backspace" mode="contained" style={styles.button} color="#9134af" onPress={() => this.props.navigation.navigate('SignIn')}> 
    Cancel
  </Button>
  </Surface>
        </PaperProvider>
      );
    }
  }
  const theme = {
    ...DefaultTheme,
    colors: {
      ...DefaultTheme.colors,
      primary: '#9134af',
      accent: '#ff5252',
    },
  };
  const styles = StyleSheet.create({
    bottom: {
      position: 'absolute',
      left: 0,
      right: 0,
      bottom: 0,
    },
    text:{
      width:'95%',
      borderRadius:0,
      backgroundColor:"white",
      marginBottom:10
    }
    ,
    button:{
      color:'white',
      marginTop:10,
      width:'95%'
    },
    surface: {
      padding: 8,
      marginTop:10,
      //height: 80,
      width: '100%',
      alignItems: 'center',
      justifyContent: 'center',
      elevation: 0,
    },
  });
