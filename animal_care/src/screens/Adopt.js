import React from 'react';
import { withNavigation } from 'react-navigation';
import { Platform, StatusBar, StyleSheet, View,Text,ScrollView,Dimensions ,FlatList } from 'react-native';
import { DefaultTheme, Provider as PaperProvider } from 'react-native-paper';
import { Appbar } from 'react-native-paper';
import { TextInput,Title,Button,Surface,Badge } from 'react-native-paper';
import { Avatar,Divider,Drawer } from 'react-native-paper';
import { List } from 'react-native-paper';
import {Link} from './global';
class App extends React.Component {
  static navigationOptions = {
    header: null,
  };
  state = {
    isLoadingComplete: false,
    text:'',
    user:'',
    count : 1,
    data : [],
    active:'first'
  };

  render() {

    trySignup = () =>
    {
      console.log("opening signup");
      //this.props.signUp();
      this.props.navigation.navigate('Choose')
    }
    tryLogin  = () =>{
    console.log("Login Function Called");
    fetch(`${Link}/user/allSell`, {
      method: 'GET',
     
      
    })
    
      .then((response) => response.json())
      .then((responseJson) => {
          console.log(responseJson, 'res JSON');
          //alert(responseJson.message);
          //alert("done");
          console.log(this.state.user);
          console.log(this.state.password);
          this.setState({data : responseJson , text : "ibrahim" })
          
         
      })
      .catch((error) => {
          console.error(error);
      });
      }



    const { active } = this.state;
    const screenHeight = Dimensions.get('window').height;
   
    let check;
    
    if ( this.state.count === 1)
    {
      tryLogin();
      this.state.count++;
    }
    
    if (this.state.text === 'ibrahim')
    {
      tryLogin();

      check= <View style={styles.container}>
      <FlatList
         data={this.state.data}
        renderItem={({item}) =>  
        
        <List.Item
          title={item.pet_name}
          description={`Contact : ${item.conatct}\nAddress : ${item.address}`}
          left={() => <Avatar.Image size={70} source={{ uri: `http://192.168.16.102:8080/organization/Image/${item.petImage}` }}  />}
          right={()=><Badge size={27} style={{backgroundColor:'#9134af'}}>$ {item.price}</Badge>}
         // onPress={()=> this.props.navigation.navigate('OrganizationView',{ids:item._id})}
       />
      }
      />
    </View>

    }



      return (
        <PaperProvider theme={theme} style={{width:'80%',height:screenHeight}}>
        
      
        <Surface style={styles.surface}>
        <Title style={{color:"#9134af"}}>Adopt </Title>
        <Divider/>
        <ScrollView 
    behaviour = "height"
    keyboardVerticalOffset = {64}
    style= {{ marginTop: '0%',width:"100%",height:"100%",padding:5}}
  >
        
{check}
  
  <Button icon="backspace" mode="contained" style={styles.button} color="#9134af" onPress={() => this.props.navigation.navigate('Dashboard')}>
      
      Cancel
    </Button>
  
 <View style={{padding:50}}></View>

  </ScrollView>
  </Surface>
        </PaperProvider>
      );
    }
  }
  export default withNavigation(App);
  const theme = {
    ...DefaultTheme,
    colors: {
      ...DefaultTheme.colors,
      primary: '#9134af',
      accent: '#ff5252',
    },
  };
  const styles = StyleSheet.create({
    bottom: {
      position: 'absolute',
      left: 0,
      right: 0,
      bottom: 0,
    },
    text:{
      width:'95%',
      borderRadius:0,
      backgroundColor:"white",
      marginBottom:10
    }
    ,
    item: {
      padding: 10,
      fontSize: 18,
      height: 44,
    },
    button:{
      color:'white',
      marginTop:10,
      width:'95%'
    },
    surface: {
      padding: 8,
      marginTop:10,
      paddingBottom:25,
      //height: 80,
      width: '100%',
      alignItems: 'center',
      justifyContent: 'center',
      elevation: 0,
    },
  });
