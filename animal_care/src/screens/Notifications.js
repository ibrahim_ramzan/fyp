import * as React from 'react';
import { BottomNavigation, Text,DefaultTheme } from 'react-native-paper';
import Emergency from './Emergency';
import Appoint from './Appoint';
import { withNavigation } from 'react-navigation';

const Emergenc = () =><Emergency/>;

const Appointment = () => <Appoint/>;



 class App extends React.Component {
     
  state = {
    index: 0,
    routes: [
      { key: 'nearest', title: 'Emergency', icon: 'local-hospital',color:'#9134af' },
      { key: 'list', title: 'Appointments', icon: 'access-time',color:'#9134af' },
    ],
  };
  
  openOrganization=() =>
{
  alert("Called Open Single Organization");
  //this.props.navigation.navigate('OrganizationView')
}
  _handleIndexChange = index => this.setState({ index });

  _renderScene = BottomNavigation.SceneMap({
    nearest: Emergenc,
    list: Appointment,
  });

  render() {
    return (
      <BottomNavigation style={{backgroundColor:"#9134af"}}
        navigationState={this.state}
        onIndexChange={this._handleIndexChange}
        renderScene={this._renderScene}
        theme={theme}
        color="#9134af"
      />
    );
  }
}
export default withNavigation(App);
const theme = {
    ...DefaultTheme,
    colors: {
      ...DefaultTheme.colors,
      primary: '#9134af',
      accent: '#ff5252',
    },
  };