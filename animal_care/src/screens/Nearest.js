import * as React from 'react';
import { BottomNavigation, Text,DefaultTheme } from 'react-native-paper';
import Map from './redux_check/SharePlace';
import Organizations from './ListOrganizations';
const Nearest = () =><Map/>;

const List = () => <Organizations/>;

export default class MyComponent extends React.Component {
  state = {
    index: 0,
    routes: [
      { key: 'nearest', title: 'Nearest Vet', icon: 'my-location',color:'#9134af' },
      { key: 'list', title: 'Organizations', icon: 'account-balance',color:'#9134af' },
      
    ],
  };
  openOrganization=() =>
{
  alert("Called Open Single Organization");
  //this.props.navigation.navigate('OrganizationView')
}
  _handleIndexChange = index => this.setState({ index });

  _renderScene = BottomNavigation.SceneMap({
    nearest: Nearest,
    list: List,
  });

  render() {
    return (
      <BottomNavigation style={{backgroundColor:"#9134af"}}
        navigationState={this.state}
        onIndexChange={this._handleIndexChange}
        renderScene={this._renderScene}
        theme={theme}
        color="#9134af"
      />
    );
  }
}
const theme = {
    ...DefaultTheme,
    colors: {
      ...DefaultTheme.colors,
      primary: '#9134af',
      accent: '#ff5252',
    },
  };