import React from 'react';
import { Platform, StatusBar, StyleSheet, View,Text } from 'react-native';
import { Button,DefaultTheme, Provider as PaperProvider } from 'react-native-paper';
import { Appbar } from 'react-native-paper';
import { TextInput,Title,Surface } from 'react-native-paper';
import { Avatar,Divider,Drawer } from 'react-native-paper';
import { IconButton, Colors,Chip } from 'react-native-paper';
import { Searchbar } from 'react-native-paper';
export default class App extends React.Component {
  static navigationOptions = {
    header: null,
  };
  constructor(props) {
    super(props);
 this.state = {
  firstQuery: '',
    isLoadingComplete: false,
    text:'',
    user:'',
    active:'first'
  };
}

  logout = () =>
  {
    console.log("presses log out");
    this.props.navigation.navigate('SignIn')
  }
  displayMap= () => {
    console.log("calling prop");
    this.props.navigation.navigate('Share')
  }
  render() {
    const { active } = this.state;
    const { firstQuery } = this.state;
      return (
        <PaperProvider theme={theme} style={{width:'80%'}}>
        <Appbar.Header>
        <Avatar.Icon size={50} style={{marginRight:2}} icon="beach-access" />
        <Appbar.Content
          title="Save Tails" style={{marginLeft:2}}
        />
        <Appbar.Action icon="person" onPress={() =>  this.props.navigation.navigate('UserProfile')} />
       <Appbar.Action icon="settings-power" onPress={() =>  this.props.navigation.navigate('SignIn')} />
      </Appbar.Header>
        
      
        <Surface style={styles.surface}>
        <Chip style={{alignItems:'center',borderColor:'#9134af',borderRadius:0,borderWidth:3,fontSize:20,color:'#9134af',marginTop:10,marginBottom:10}} Type="outlined"><Text style={{fontSize:20,color:'#9134af',fontWeight: 'bold'}}>Wildlife</Text></Chip>
        
        <View style={styles.container}>
              <View style={styles.buttonContainer}>
              <Button icon="notifications" style={{marginRight:8,height:100,justifyContent:'center',fontSize:45,marginTop:10}} mode="contained" onPress={() => console.log('Pressed')} onPress={() =>  this.props.navigation.navigate('NotificationsListOrg')}>
              
               Notifications
  </Button>
              </View>
              
            </View>


            

            

            
            
  
  
  </Surface>
        </PaperProvider>
      );
    }
  }
  const theme = {
    ...DefaultTheme,
    colors: {
      ...DefaultTheme.colors,
      primary: '#9134af',
      accent: '#ff5252',
    },
  };
  const styles = StyleSheet.create({
    container: {
        //flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    buttonContainer: {
        flex: 1,
    },
    bottom: {
      position: 'absolute',
      left: 0,
      right: 0,
      bottom: 0,
    },
    text:{
      width:'95%',
      borderRadius:0,
      backgroundColor:"white",
      marginBottom:10
    }
    ,
    button:{
      color:'white',
      marginTop:10,
      width:'95%'
    },
    surface: {
      padding: 8,
      marginTop:0,
     // height: '100%',
      width: '100%',
     // alignItems: 'left',
      justifyContent: 'center',
      elevation: 0,
    },
  });
