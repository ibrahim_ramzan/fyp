import React from 'react';
import { Platform, StatusBar, StyleSheet, View,Text,ScrollView,Dimensions } from 'react-native';
import { DefaultTheme, Provider as PaperProvider } from 'react-native-paper';
import { Appbar } from 'react-native-paper';
import { TextInput,Title,Button,Surface } from 'react-native-paper';
import { Avatar,Divider,Drawer } from 'react-native-paper';
import { List } from 'react-native-paper';
import {Link} from './global';
import {   Card,  Paragraph } from 'react-native-paper';
export default class App extends React.Component {
  static navigationOptions = {
    header: null,
  };
  state = {
    isLoadingComplete: false,
    text:'',
    user:'',
    active:'first',
    data:{}
    
  };
  componentDidMount()
{
      
      fetch(`${Link}/organization/getone`, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          id:this.props.navigation.getParam('ids',123)
        }),
       
      })
      
        .then((response) => response.json())
        .then((responseJson) => {
          //alert(responseJson.company_name)
          this.setState({
            data:responseJson
          })
        })
        .catch((error) => {
            console.error(error);
        });
}
  render() {
  
    const { active } = this.state;
  const ip = this.props.navigation.getParam('ids',123);
    const screenHeight = Dimensions.get('window').height;
      return (
        <PaperProvider theme={theme} style={{width:'80%',height:screenHeight}}>
        <Appbar.Header>
        <Avatar.Icon size={50} style={{marginRight:2}} icon="beach-access" />
        <Appbar.Content
          title="Save Tails" style={{marginLeft:2}}
        />
       
      </Appbar.Header>
        
      
        <Surface style={styles.surface}>
        
        <ScrollView 
    behaviour = "height"
    keyboardVerticalOffset = {64}
    style= {{ marginTop: '5%',width:"100%",height:"100%",padding:5}}
  >
        
        
     
        <Card>
    
    
    <Card.Cover source={{ uri: 'https://picsum.photos/700' }} />
    <Card.Content>
      <Title>{this.state.data.company_name}</Title>
      <Paragraph>Description : {this.state.data.bio} </Paragraph>
      <Paragraph>Address : {this.state.data.address}</Paragraph>
      <Paragraph>City : {this.state.data.city}</Paragraph>
      <Paragraph>Contact : {this.state.data.phone_no}</Paragraph>
      <Paragraph>Email : {this.state.data.email}</Paragraph>
      <Paragraph>Website : {this.state.data.webiste}</Paragraph>
    </Card.Content>
    
  </Card>

  

  <Button icon="backspace" mode="contained" style={styles.button} color="#9134af" onPress={() => this.props.navigation.navigate('Nearest')}>
      
    Back
  </Button>
 <View style={{padding:50}}></View>

  </ScrollView>
  </Surface>
        </PaperProvider>
      );
    }
  }
  const theme = {
    ...DefaultTheme,
    colors: {
      ...DefaultTheme.colors,
      primary: '#9134af',
      accent: '#ff5252',
    },
  };
  const styles = StyleSheet.create({
    bottom: {
      position: 'absolute',
      left: 0,
      right: 0,
      bottom: 0,
    },
    text:{
      width:'95%',
      borderRadius:0,
      backgroundColor:"white",
      marginBottom:10
    }
    ,
    button:{
      color:'white',
      marginTop:10,
      width:'95%'
    },
    surface: {
      padding: 8,
      marginTop:10,
      paddingBottom:25,
      //height: 80,
      width: '100%',
      alignItems: 'center',
      justifyContent: 'center',
      elevation: 0,
    },
  });
