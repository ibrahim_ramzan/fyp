
import React from 'react';
import { Platform, StatusBar, StyleSheet, View,Text ,ImageBackground,NetInfo} from 'react-native';
import { DefaultTheme, Provider as PaperProvider } from 'react-native-paper';
import { Appbar } from 'react-native-paper';
import { TextInput,Title,Button,Surface } from 'react-native-paper';
import { Avatar,Divider,Drawer } from 'react-native-paper';
import backgroundImage from "../../assets/background.jpeg";
import { RadioButton } from 'react-native-paper';
import { ScrollView } from 'react-native-gesture-handler';
import {Link} from '../global';

class HomeScreen extends React.Component {

  static navigationOptions = {
    header: null,
  };
    constructor(props) {
        super(props); 
      this.state = {
        checked: 'user',
        isLoadingComplete: false,
        text:'',
        user:'User@gmail.com',//'talha@gmail.com',
        password :'123456',//'talha',
        connection_Status:''
       
      };
    }
    trySignup = () =>
    {
      console.log("opening signup");
      //this.props.signUp();
      this.props.navigation.navigate('Choose')
    }
    tryLogin  = () =>{
     // alert(Link)
    //alert("Login Function Called");
    if(this.state.checked == "user")
    {
            var log = "user";
    }
    else{
      log="organization";
    }
    fetch(`${Link}/${log}/login`, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        email: this.state.user,
        password: this.state.password,
      }),
    })
    
      .then((response) => response.json())
      .then((responseJson) => {
          console.log(responseJson, 'res JSON');
          //alert(responseJson.message);
          console.log(this.state.user);
          console.log(this.state.password);

          if(this.state.checked == "user")
          {
            if(responseJson.message == "Auth successful")
              {
                
                this.props.navigation.navigate('Home',{id:responseJson.id})
              }
              else{
                alert("Login Failed")
                //this.props.navigation.navigate('Home',{id:'545478'})
              }
          }
          else{
            if(responseJson.message == "Auth successful")
              {
                this.props.navigation.navigate('OrgHome',{id:responseJson.key})
                //alert(responseJson.key)
              }
              else{
                alert("Login Failed")
              }
            //this.props.navigation.navigate('OrgHome')

          }
          
         /* if(responseJson.message == "Auth successful")
          {
            //alert(responseJson.id);
            //this.props.updateLogin();
            if(this.state.checked == "user")
            {
            this.props.navigation.navigate('Home',{id:responseJson.id})
            }
            else{
              this.props.navigation.navigate('OrgHome')
              //this.props.navigation.navigate('RescueOrg')
              //this.props.navigation.navigate('WildLife')
            }
          }
          else{
            alert("Invalid Username/Password");
          }*/
      })
      .catch((error) => {
          console.error(error);
      });
      }

      //internet check
      componentDidMount() {
 
        NetInfo.isConnected.addEventListener(
            'connectionChange',
            this._handleConnectivityChange
     
        );
       
        NetInfo.isConnected.fetch().done((isConnected) => {
     
          if(isConnected == true)
          {
            this.setState({connection_Status : "Online"})
          }
          else
          {
            this.setState({connection_Status : "Offline"})
          }
     
        });
      }
      
     
      componentWillUnmount() {
     
        NetInfo.isConnected.removeEventListener(
            'connectionChange',
            this._handleConnectivityChange
     
        );
     
      }
     
      _handleConnectivityChange = (isConnected) => {
     
        if(isConnected == true)
          {
            this.setState({connection_Status : "Online"})
          }
          else
          {
            this.setState({connection_Status : "Offline"})
          }
      };
      //internet check
      render() {
        const { firstQuery } = this.state;
        const { checked } = this.state;

        
          return (
            
            <PaperProvider theme={theme} style={{width:'80%'}}>
           
            
            <Appbar.Header>
            <Appbar.Content
              title="Save Tails"
            />
          </Appbar.Header>
            
          <ScrollView>
            <Surface style={styles.surface}>
            <Avatar.Image size={160} source={require('../../assets/app.jpg')}  style={{borderRadius:0,backgroundColor:"#fff"}}/>
            <Title style={{color:"#9134af"}}>Login- Save Tails</Title>
    
          <TextInput style={styles.text}
            label='Email'
            Type="outlined"
            value={this.state.user}
            onChangeText={user => this.setState({ user })}
          />
        
          <TextInput style={styles.text}
            label='Password'
            secureTextEntry={true}
            Type="outlined"
            value={this.state.password}
            onChangeText={password => this.setState({ password })}
          />

<View>
  <View style={{display: 'flex', flexDirection: 'row'}}>
        <RadioButton
          value="user"
          status={checked === 'user' ? 'checked' : 'unchecked'}
          onPress={() => { this.setState({ checked: 'user' }); }}
          color="#9134af"/> 

      <Text onPress={() => { this.setState({ checked: 'user' }); }} style={{fontSize:20}}>User</Text></View>
      <View style={{display: 'flex', flexDirection: 'row'}}>
        <RadioButton
          value="organization"
          status={checked === 'organization' ? 'checked' : 'unchecked'}
          onPress={() => { this.setState({ checked: 'organization' }); }}
          color="#9134af"
        /> 
        <Text onPress={() => { this.setState({ checked: 'organization' }); }} style={{fontSize:20}}>Organization</Text>
        </View>
      </View>
          <Text style={{color:"#9134af",margin:10,}}>Forgot Password ?</Text>
          <Button icon="navigate-next" mode="contained" style={styles.button} color="#9134af" onPress={this.tryLogin}>
          
        Login
      </Button>
    
      <Button icon="add-circle-outline" mode="contained" style={styles.button} color="#9134af" onPress={this.trySignup}>
          
        Create New Account
      </Button>
      
     
      </Surface>
      </ScrollView>
            </PaperProvider>
           

           
          );
          
        }


}

const theme = {
    ...DefaultTheme,
    colors: {
      ...DefaultTheme.colors,
      primary: '#9134af',
      accent: '#ff5252',
    },
  };
  const styles = StyleSheet.create({
    bottom: {
      position: 'absolute',
      left: 0,
      right: 0,
      bottom: 0,
    },
    text:{
      width:'95%',
      borderRadius:0,
      backgroundColor:"white",
      marginBottom:10
    }
    ,

    backgroundImage: {
      width: "100%",
      flex: 1
    },
    button:{
      color:'white',
      marginTop:10,
      width:'95%'
    },
    surface: {
      padding: 8,
      marginTop:40,
      //height: 80,
      width: '100%',
      alignItems: 'center',
      justifyContent: 'center',
      elevation: 0,
    },
  });


export default HomeScreen;