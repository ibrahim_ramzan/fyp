/*This is an example of Image Picker in React Native*/
import React from 'react';
import {   View, Image } from 'react-native';
import { Platform, StatusBar, StyleSheet,Text,ScrollView,Dimensions } from 'react-native';
import { DefaultTheme, Provider as PaperProvider } from 'react-native-paper';
import { Appbar } from 'react-native-paper';
import { TextInput,Title,Button,Surface } from 'react-native-paper';
import { Avatar,Divider,Drawer } from 'react-native-paper';
var ImagePicker = require('react-native-image-picker');
import {Link} from './global';
export default class App extends React.Component {
    
  constructor(props) {
    super(props);
    this.state = {
      filePath: {},
      isLoadingComplete: false,
    text:'',
    user:'',
    description : '',
    phoneNo : '',
    id :'5c96770a78e7972194afe562',
    active:'first',
    pet_name : "",
    pet_description : "",
    conatct : "",
    address : "",
     data : new FormData(),
    
    latitude: null,
      longitude: null,
      error:null,
    };
  }
  

  trySignUp  = (st) =>{
    this.state.data.append('pet_name',this.state.pet_name)
        this.state.data.append('pet_description',this.state.pet_description)
        this.state.data.append('conatct',this.state.conatct)
        this.state.data.append('address',this.state.address)
        this.state.data.append('id',"5ce5f1425b449f19b873cef2")
    console.log("Create Account Function Called");
    fetch(`${Link}/user/addPetToDonate`, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'multipart/form-data',
      },
      body: this.state.data

      //data : this.state.data,
      
    })
      .then((response) => response.json())
      .then((responseJson) => {
          console.log(responseJson, 'res JSON');
          //alert(responseJson.message);
          console.log(this.state.user);
          console.log(this.state.password);
          
          if(responseJson.message=="done")
          {
            alert("Request Submitted Successfully");
          }
          else{
            alert("Failed")
          }
         // alert("Request Submitted Successfully");
            
          
      })
      .catch((error) => {
          console.error(error);
          alert(error);
      });
      }




  chooseFile = () => {
    var options = {
      title: 'Select Image',
      customButtons: [
        { name: 'customOptionKey', title: 'Choose Photo from Custom Option' },
      ],
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };
    ImagePicker.showImagePicker(options, response => {
      console.log('Response = ', response);
 
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
        alert(response.customButton);
      } else {
        let source = response;
        // You can also display the image using data:
        // let source = { uri: 'data:image/jpeg;base64,' + response.data };
        
        
        
        this.state.data.append('name', 'Emergency');

        this.state.data.append('DonateImage', {
         uri : response.uri,
         type: response.type,
         name: response.fileName,
        });
        
        this.setState({
          filePath: source,
        });
      }
    });
  };
  render() {
    const { active } = this.state;
    const screenHeight = Dimensions.get('window').height;
    return (
      
      <PaperProvider theme={theme} style={{width:'80%',height:screenHeight}}>
      <Appbar.Header>
      <Avatar.Icon size={50} style={{marginRight:2}} icon="beach-access" />
      <Appbar.Content
        title="Save Tails" style={{marginLeft:2}}
      />
     
    </Appbar.Header>
      
    
      <Surface style={styles.surface}>
      <Title style={{color:"#9134af"}}>Donate Pet </Title>
      <ScrollView 
  behaviour = "height"
  keyboardVerticalOffset = {64}
  style= {{ marginTop: '0%',width:"100%",height:"100%",padding:5}}
>
            
          
          <Button icon="add-circle-outline" mode="contained" style={styles.button} color="#9134af" onPress={this.chooseFile.bind(this)}>
    
  Attach Picture
</Button>
<Image
            source={{
              uri: 'data:image/jpeg;base64,' + this.state.filePath.data,
            }}
            style={{ width: 100, height: 100 }}
          />

      <TextInput style={styles.text}
      label='Enter Your Pet Name'
      Type="outlined"
      value={this.state.phoneNo}
      onChangeText={phoneNo => this.setState({ phoneNo })}
    />
      <TextInput style={styles.text}
      label='Description'
      Type="outlined"
      value={this.state.pet_description}
      onChangeText={pet_description => this.setState({ pet_description })}
    />

<TextInput style={styles.text}
      label='conatct'
      Type="outlined"
      value={this.state.conatct}
      onChangeText={conatct => this.setState({ conatct })}
    />

<TextInput style={styles.text}
      label='address'
      Type="outlined"
      value={this.state.address}
      onChangeText={address => this.setState({ address })}
    />
   
   


<Button icon="add-circle-outline" mode="contained" style={styles.button} color="#9134af" onPress={()=> this.trySignUp(1)}>
    
  Submit Post
</Button>

<Button icon="backspace" mode="contained" style={styles.button} color="#9134af" onPress={() => this.props.navigation.navigate('Home')}>
    
  Cancel
</Button>
<View style={{padding:50}}></View>

</ScrollView>
</Surface>
      </PaperProvider>
    );
  }
}
const theme = {
    ...DefaultTheme,
    colors: {
      ...DefaultTheme.colors,
      primary: '#9134af',
      accent: '#ff5252',
    },
  };
  const styles = StyleSheet.create({
    bottom: {
      position: 'absolute',
      left: 0,
      right: 0,
      bottom: 0,
    },
    text:{
      width:'95%',
      borderRadius:0,
      backgroundColor:"white",
      marginBottom:10
    }
    ,
    button:{
      color:'white',
      marginTop:10,
      width:'95%'
    },
    surface: {
      padding: 8,
      marginTop:10,
      paddingBottom:25,
      //height: 80,
      width: '100%',
      alignItems: 'center',
      justifyContent: 'center',
      elevation: 0,
    },
  });
