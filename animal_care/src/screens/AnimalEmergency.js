/*This is an example of Image Picker in React Native*/
import React from 'react';
import {   View, Image } from 'react-native';
import { Platform, StatusBar, StyleSheet,Text,ScrollView,Dimensions } from 'react-native';
import { DefaultTheme, Provider as PaperProvider,RadioButton } from 'react-native-paper';
import { Appbar } from 'react-native-paper';
import { TextInput,Title,Button,Surface } from 'react-native-paper';
import { Avatar,Divider,Drawer } from 'react-native-paper';
import {Link} from './global';
var ImagePicker = require('react-native-image-picker');
export default class App extends React.Component {
    componentDidMount() {
        navigator.geolocation.getCurrentPosition(
           (position) => {
             console.log("wokeeey");
             console.log(position);
             this.setState({
               latitude: position.coords.latitude,
               longitude: position.coords.longitude,
               error: null,
             });
           },
           (error) => this.setState({ error: error.message }),
           { enableHighAccuracy: false, timeout: 200000, maximumAge: 1000 },
         );
       }

  constructor(props) {
    super(props);
    this.state = {
      filePath: {},
      isLoadingComplete: false,
    text:'',
    user:'',
    checked:'user',
    description : '',
    phoneNo : '',
    id :'5c96770a78e7972194afe562',
    active:'first',
     data : new FormData(),
    
    latitude: null,
      longitude: null,
      error:null,
    };
  }
  

  trySignUp  = () =>{
    this.state.data.append('latitude',this.state.latitude)
        this.state.data.append('longitude',this.state.longitude)
        this.state.data.append('description',this.state.description)
        this.state.data.append('phoneNo',this.state.phoneNo)
        this.state.data.append('id',this.props.navigation.state.params.id)
    console.log("Create Account Function Called");
    fetch(`${Link}/user/Emergency`, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'multipart/form-data',
      },
      body: this.state.data

      //data : this.state.data,
      
    })
      .then((response) => response.json())
      .then((responseJson) => {
          console.log(responseJson, 'res JSON');
          //alert(responseJson.message);
         
          
          if(responseJson.message=="done")
          {
            alert("Request Submitted Successfully");
          }
          else{
            alert("Failed")
          }
            
          
      })
      .catch((error) => {
          console.error(error);
          alert(error);
      });
      }




  chooseFile = () => {
    var options = {
      title: 'Select Image',
      customButtons: [
        { name: 'customOptionKey', title: 'Choose Photo from Custom Option' },
      ],
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };
    ImagePicker.showImagePicker(options, response => {
      console.log('Response = ', response);
 
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
        alert(response.customButton);
      } else {
        let source = response;
        // You can also display the image using data:
        // let source = { uri: 'data:image/jpeg;base64,' + response.data };
        
        
        
        this.state.data.append('name', 'Emergency');

        this.state.data.append('Emergency', {
         uri : response.uri,
         type: response.type,
         name: response.fileName,
        });
        
        this.setState({
          filePath: source,
        });
      }
    });
  };
  render() {
    const { active } = this.state;
    const { checked } = this.state;
    const screenHeight = Dimensions.get('window').height;
    return (
      
      <PaperProvider theme={theme} style={{width:'80%',height:screenHeight}}>
      <Appbar.Header>
      <Avatar.Icon size={50} style={{marginRight:2}} icon="beach-access" />
      <Appbar.Content
        title="Save Tails" style={{marginLeft:2}}
      />
     
    </Appbar.Header>
      
    
      <Surface style={styles.surface}>
      <Title style={{color:"#9134af"}}>Emergency </Title>
      <ScrollView 
  behaviour = "height"
  keyboardVerticalOffset = {64}
  style= {{ marginTop: '0%',width:"100%",height:"100%",padding:5}}
>
            
          
          <Button icon="add-circle-outline" mode="contained" style={styles.button} color="#9134af" onPress={this.chooseFile.bind(this)}>
    
  Attach Picture
</Button>
<Image
            source={{
              uri: 'data:image/jpeg;base64,' + this.state.filePath.data,
            }}
            style={{ width: 100, height: 100 }}
          />

      <TextInput style={styles.text}
      label='Enter Your Phone Number'
      Type="outlined"
      value={this.state.phoneNo}
      onChangeText={phoneNo => this.setState({ phoneNo })}
    />
      <TextInput style={styles.text}
      label='Description'
      Type="outlined"
      value={this.state.description}
      onChangeText={description => this.setState({ description })}
    />

<View>
  <View style={{display: 'flex', flexDirection: 'row'}}>
        <RadioButton
          value="user"
          status={checked === 'user' ? 'checked' : 'unchecked'}
          onPress={() => { this.setState({ checked: 'user' }); }}
          color="#9134af"/> 
      <Text onPress={() => { this.setState({ checked: 'user' }); }} style={{fontSize:20}}>Endangered Species</Text></View>
      <View style={{display: 'flex', flexDirection: 'row'}}>
        <RadioButton
          value="user"
          status={checked === 'user1' ? 'checked' : 'unchecked'}
          onPress={() => { this.setState({ checked: 'user1' }); }}
          color="#9134af"/> 
      <Text onPress={() => { this.setState({ checked: 'user1' }); }} style={{fontSize:20}}>Animal Abuse</Text></View>
      <View style={{display: 'flex', flexDirection: 'row'}}>
        <RadioButton
          value="organization"
          status={checked === 'organization' ? 'checked' : 'unchecked'}
          onPress={() => { this.setState({ checked: 'organization' }); }}
          color="#9134af"
        /> 
        <Text onPress={() => { this.setState({ checked: 'organization' }); }} style={{fontSize:20}}>Rescue</Text>
        </View>
        <View style={{display: 'flex', flexDirection: 'row'}}>
        <RadioButton
          value="organization1"
          status={checked === 'organization1' ? 'checked' : 'unchecked'}
          onPress={() => { this.setState({ checked: 'organization1' }); }}
          color="#9134af"
        /> 
        <Text onPress={() => { this.setState({ checked: 'organization1' }); }} style={{fontSize:20}}>Animal Burial</Text>
        </View>
      </View>

   <Text>Current Location : </Text>
   <Text>{this.state.longitude} , {this.state.latitude}</Text>
   


<Button icon="add-circle-outline" mode="contained" style={styles.button} color="#9134af" onPress={this.trySignUp}>
    
  Submit Emergency
</Button>

<Button icon="backspace" mode="contained" style={styles.button} color="#9134af" onPress={() => this.props.navigation.navigate('Home')}>
    
  Cancel
</Button>
<View style={{padding:50}}></View>

</ScrollView>
</Surface>
      </PaperProvider>
    );
  }
}
const theme = {
    ...DefaultTheme,
    colors: {
      ...DefaultTheme.colors,
      primary: '#9134af',
      accent: '#ff5252',
    },
  };
  const styles = StyleSheet.create({
    bottom: {
      position: 'absolute',
      left: 0,
      right: 0,
      bottom: 0,
    },
    text:{
      width:'95%',
      borderRadius:0,
      backgroundColor:"white",
      marginBottom:10
    }
    ,
    button:{
      color:'white',
      marginTop:10,
      width:'95%'
    },
    surface: {
      padding: 8,
      marginTop:10,
      paddingBottom:25,
      //height: 80,
      width: '100%',
      alignItems: 'center',
      justifyContent: 'center',
      elevation: 0,
    },
  });
