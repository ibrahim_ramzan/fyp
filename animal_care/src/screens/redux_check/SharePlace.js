/*import React, { Component } from 'react';
import { AppRegistry, StyleSheet,SearchBar, View, Dimensions ,Button,TextInput,Text } from 'react-native';
import MapView, { PROVIDER_GOOGLE ,Callout} from 'react-native-maps';
import { connect } from 'react-redux';
//import console = require('console');
//import RetroMapStyles from './MapStyles/RetroMapStyles.json';
let { width, height } = Dimensions.get('window');
const ASPECT_RATIO = width / height;
const LATITUDE = 0;
const LONGITUDE = 0;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
class SharePlaceScreen extends Component {
  
      

      state = {
          focusdLocation : {
              latitude : 37.7900352,
              longitude : -122.4013726,
              latitudeDelta : 0.0122,
              longitudeDelta : 
                Dimensions.get("window").width / 
                Dimensions.get("window").height * 
                0.0122
          },
          locationChosen : false
      }
      

      pickLocationHandler = event => {
          const coords = event.nativeEvent.coordinate;
          this.map.animateToRegion({
              ...this.state.focusdLocation,
              latitude : coords.latitude,
              longitude : coords.longitude
          })
          this.setState(prevState => {
              return {
                  focusdLocation : {
                      ...prevState.focusdLocation,
                      latitude : coords.latitude,
                      longitude : coords.longitude
                  },
                  locationChosen : true

              };
          })
      }

      getLocationHandler = () => {
          navigator.geolocation.getCurrentPosition(pos => {
              const coordsEvent = {
                  nativeEvent : {
                      coordinate : {
                          latitude : pos.coords.latitude,
                          longitude : pos.coords.longitude
                      }
                  }
              };
              this.pickLocationHandler(coordsEvent);
          },
          err=> {
              console.log(err);
              alert("Fetching The Position Failed!");
          });
      }

      render() {
          
         let marker = null;
          if ( this.state.locationChosen)
          {
              marker = <MapView.Marker coordinate={this.state.focusdLocation} />
          }
         

        return (
           
            <View style={{ flex: 1 }}>
            <MapView
              style={{ flex: 1 }}
              initialRegion={this.state.focusdLocation}
              // region = {this.state.focusdLocation}
             
               onPress = {this.pickLocationHandler}
               ref = {ref => this.map = ref}
               
            >
             {marker}
        
        </MapView>
           
            <View
                style={{
                    position: 'absolute',//use absolute position to show button on top of the map
                    top: '50%', //for center align
                    alignSelf: 'flex-end' //for align to right
                }}
            >
                 <Button title="Locate Me" onPress={this.getLocationHandler} />
            </View>
        </View>
          
        );
      }
}


const styles = StyleSheet.create({
    container: {
      flexDirection : 'row',
      height: '100%',
      width: '100%',
    },

    calls : {
      marginRight : 50,
      marginTop : 500,

    },

    search : {
      
        height : 80,
        borderWidth : 0.5,
        marginTop : 120,
        marginLeft : 5,
        marginRight : 100,
        padding : 5,
        backgroundColor : "black",
        flex : 1,
        justifyContent : 'center',
        alignItems : 'center'

    },



    calloutView: {
      flexDirection: 'row',
      backgroundColor: "rgba(255, 255, 255, 0.9)",
      borderRadius: 10,
      width: "40%",
      marginLeft: "30%",
      marginRight: "30%",
      marginTop: 20
    },

    calloutSearch: {
      borderColor: "transparent",
      marginleft: 10,
      width: "90%",
      marginRight: 10,
      height: 40,
      borderWidth: 0.0  
    },

    map : {
        
        width : "100%",
        height : "100%"
    },

    calloutView: {
        flexDirection: "row",
        backgroundColor: "rgba(255, 255, 255, 0.9)",
        borderRadius: 10,
        width: "40%",
        //marginLeft: "30%",
        //marginRight: "30%",
        marginTop: 20
      },
      calloutSearch: {
        borderColor: "transparent",
        //marginleft: 10,
        width: "90%",
       // marginRight: 10,
        height: 40,
        borderWidth: 0.0  
      }
  });

const mapDispatchToProps = dispatch => {
    return {
        onAddPlace: (placeName) => dispatch(addPlace(placeName))
    };
};*/
import React, { Component } from "react";
import {
  StyleSheet,
  TextInput,
  View,
  Text,
  TouchableHighlight,
  Keyboard
} from "react-native";
import MapView from "react-native-maps";
//import apiKey from "../google_api_key";
import _ from "lodash";
import { Marker } from 'react-native-maps';
import { connect } from 'react-redux';
export default class App extends React.Component {

    static navigationOptions = {
        header: null,
      };

        state = {
          error: "",
          latitude: 73.084488,
          longitude: 33.738045,
          locationPredictions: [],
          check : false,
          data:{},
          display:false,
          lng:33.738045,
          lt:73.084488
        };
       
        
      
      loadVets = () => {
        
        fetch(`https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=${this.state.lng},${this.state.lt}&radius=5000&type=pets&keyword=vets&key=AIzaSyD4TOFRjbg3qPGHh4j8YeconpEghBed5WM`)
        .then((response) => response.json())
        .then((responseJson) => {
           this.setState({
            data:responseJson.results,
            display:true
          })
         
          //alert(this.state.data[0].geometry.location.lat)
        })
        .catch((error) => {
          console.log(error);
        });
        
      }
      
      componentDidMount() {
        //Get current location and set initial region to this
        navigator.geolocation.getCurrentPosition(
          position => {
            
            this.setState({
              latitude: position.coords.latitude,
              longitude: position.coords.longitude
            });
            
          },
          error => this.setState({ error: error.message }),
          { enableHighAccuracy: true, maximumAge: 2000, timeout: 20000 }
        );
          this.loadVets();
      }
    
       
    
     
    
      render() {
        const mp =  this.state.display === false?<View></View>:this.state.data.map(function(data,id){
          return(
            <Marker
            coordinate={{
              latitude:data.geometry.location.lat,
              longitude:data.geometry.location.lng
            }}
            title={data.name}
            description={"Address : " + data.plus_code.compound_code}
          />
          )
        })
       
      
    
        return (
          
<View style={styles.container}>
            {this.state.display === false?<View></View>:<MapView
            style={styles.map}
            region={{
              latitude: this.state.latitude,//this.state.latitude,
              longitude:this.state.longitude,// this.state.longitude,
              latitudeDelta: 0.015,
              longitudeDelta: 0.0121
            }}
            showsUserLocation={true}
          >
     {mp}
  </MapView>}
             
          </View>
          
          
        );
      }



}

const styles = StyleSheet.create({
    destinationInput: {
      borderWidth: 0.5,
      borderColor: "grey",
      height: 40,
      marginTop: 50,
      marginLeft: 5,
      marginRight: 5,
      padding: 5,
      backgroundColor: "white"
    },
    locationSuggestion: {
      backgroundColor: "white",
      padding: 5,
      fontSize: 18,
      borderWidth: 0.5
    },
    container: {
      flex : 1,
      justifyContent : 'center',
      alignItems : 'center',
      backgroundColor: '#F5FCFF'
    },
    map: {
      position : 'absolute',
    left : 0,
    top : 0,
    right : 0,
    bottom : 0,
    }
  });

