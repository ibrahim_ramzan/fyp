import React from "react";
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  Platform,
  PermissionsAndroid
} from "react-native";
import MapView, {
  Marker,
  AnimatedRegion,
  Polyline,
  PROVIDER_GOOGLE
} from "react-native-maps";
import haversine from "haversine";

// const LATITUDE = 29.95539;
// const LONGITUDE = 78.07513;
const LATITUDE_DELTA = 0.009;
const LONGITUDE_DELTA = 0.009;
const LATITUDE = 37.78825;
const LONGITUDE = -122.4324;

class AnimatedMarkers extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      latitude: LATITUDE,
      longitude: LONGITUDE,
      routeCoordinates: [],
      distanceTravelled: 0,
      prevLatLng: {},
      coordinate: new AnimatedRegion({
        latitude: LATITUDE,
        longitude: LONGITUDE,
        latitudeDelta: 0,
        longitudeDelta: 0
      })
    };
  }

  componentDidMount() {

    this.watchId = navigator.geolocation.watchPosition(
        (position) =>  {
            this.setState({
                latitude: position.coords.latitude,
                longitude: position.coords.longitude
            });

        },
        (error) => {
            this.setState({ error:error.message})
        },
        {enableHighAccuracy:false,timeout:1 ,maximumAge:1, distanceFilter:1}
    )
  
  }

  



 

  render() {
      let initailRegion = {
          latitude : this.state.latitude,
          longitude : this.state.longitude,
          latitudeDelta : 0.01,
          longitudeDelta : 0.01
      }
      let myLocation = {latitude : this.state.latitude , longitude : this.state.longitude}
    return (
        <View style={styles.container}>
        <MapView style={styles.map} initialRegion={initailRegion} >
        
        <Marker coordinate={myLocation} />
        </MapView>

        </View>
      
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex : 1,
    justifyContent : 'center',
    alignItems : 'center',
    backgroundColor: '#F5FCFF'
  },
  map: {
    position : 'absolute',
    left : 0,
    top : 0,
    right : 0,
    bottom : 0,
  },
  bubble: {
    flex: 1,
    backgroundColor: "rgba(255,255,255,0.7)",
    paddingHorizontal: 18,
    paddingVertical: 12,
    borderRadius: 20
  },
  latlng: {
    width: 200,
    alignItems: "stretch"
  },
  button: {
    width: 80,
    paddingHorizontal: 12,
    alignItems: "center",
    marginHorizontal: 10
  },
  buttonContainer: {
    flexDirection: "row",
    marginVertical: 20,
    backgroundColor: "transparent"
  }
});

export default AnimatedMarkers;


