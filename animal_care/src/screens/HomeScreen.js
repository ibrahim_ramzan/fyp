import React from 'react';
import { Platform, StatusBar, StyleSheet, View,Text } from 'react-native';
import { Button,DefaultTheme, Provider as PaperProvider } from 'react-native-paper';
import { Appbar } from 'react-native-paper';
import { TextInput,Title,Surface } from 'react-native-paper';
import { Avatar,Divider,Drawer } from 'react-native-paper';
import { IconButton, Colors,Chip } from 'react-native-paper';
import { BottomNavigation } from 'react-native-paper';
import Dashboard from './Dashboard';
//import Map from './redux_check/SharePlace';
//import console = require('console');
const MusicRoute = () => <Dashboard displayMap={console.log("function called")}/>;

const AlbumsRoute = () => <Map/>;

const RecentsRoute = () => <Text>Recents</Text>;
showMap = () =>{
  console.log("Open Map Screen");
}
export default class MyComponent extends React.Component {
  static navigationOptions = {
    header: null,
  };
  state = {
    index: 0,
    routes: [
      { key: 'dashboard', title: 'Dashboard', icon: 'dashboard' },
      { key: 'location', title: 'My Location', icon: 'person-pin-circle' },
      { key: 'profile', title: 'Profile', icon: 'person' },
    ],
  };

  _handleIndexChange = index => this.setState({ index });

  _renderScene = BottomNavigation.SceneMap({
    dashboard: MusicRoute,
    location: AlbumsRoute,
    profile: RecentsRoute,
  });

  render() {
    return (
      <PaperProvider theme={theme} style={{width:'80%'}}>
      <BottomNavigation
        navigationState={this.state}
        onIndexChange={this._handleIndexChange}
        renderScene={this._renderScene}
      />
      </PaperProvider>
    );
  }
}

const theme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    primary: '#b33939',
    accent: '#ff5252',
  },
};