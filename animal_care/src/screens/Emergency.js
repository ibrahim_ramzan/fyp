import React from 'react';
import { withNavigation } from 'react-navigation';
import { Platform, StatusBar, StyleSheet, View,Text,ScrollView,Dimensions ,FlatList } from 'react-native';
import { DefaultTheme, Provider as PaperProvider,IconButton,Colors } from 'react-native-paper';
import { Appbar } from 'react-native-paper';
import { TextInput,Title,Button,Surface,Card,Paragraph } from 'react-native-paper';
import { Avatar,Divider,Drawer } from 'react-native-paper';
import {Link} from './global';

import { List } from 'react-native-paper';
class App extends React.Component {
  static navigationOptions = {
    header: null,
  };
  state = {
    isLoadingComplete: false,
    text:'',
    user:'',
    count : 1,
    data : [],
    active:'first',
    data:{'id':0},
  };
  delete  = (id) =>{
    alert(id)
  //alert(this.state.orgId)
  console.log("Create Account Function Called");
  fetch(`${Link}/organization/DeleteEmergency`, {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      id:id
    }),
  })
    .then((response) => response.json())
    .then((responseJson) => {
        console.log(responseJson, 'res JSON');
       
        if(responseJson.message === "Done")
        {
          //alert(responseJson.message)
          alert("Deleted Successfully");
          this.tryLogin(12);
         this.props.navigation.navigate('OrgHome');
        }
        else{
          //alert(responseJson.message);
        }
    })
    .catch((error) => {
        console.error(error);
    });
    }
  tryLogin  = (h) =>{
    // alert("Getting Notifications")
     console.log("Login Function Called");
     fetch(`${Link}/organization/SendNotification`, {
       method: 'GET',
     })
       .then((response) => response.json())
       .then((responseJson) => {
           console.log(responseJson, 'res JSON');
           //alert(responseJson.message);
           this.setState({data : responseJson , text : "ibrahim" })
       })
       .catch((error) => {
           console.error(error);
       });
       }
       componentDidMount()
       {
         this.tryLogin(1);
       }
  render() {

   



    const { active } = this.state;
    const screenHeight = Dimensions.get('window').height;
   
    let check;
    
    
  if(this.state.data.id!=0)
  {
    check = <Card>
    <Card.Content>
      <Title>Animal Emergency</Title>
      <Paragraph>Phone  : {this.state.data.phoneNo}</Paragraph>
      <Paragraph>Description : {this.state.data.description}</Paragraph>
    </Card.Content>
    <Card.Cover source={{ uri: `http://192.168.16.102:8080/organization/Image/${this.state.data.image}` }} />
    <Card.Actions>
     
    </Card.Actions>
  </Card>
  }



      return (
        <PaperProvider theme={theme} style={{width:'80%',height:screenHeight}}>
        
      
        <Surface style={styles.surface}>
        <Title style={{color:"#9134af"}}>Animal Emergency </Title>
        <ScrollView 
    behaviour = "height"
    keyboardVerticalOffset = {64}
    style= {{ marginTop: '0%',width:"100%",height:"100%",padding:5}}
  >
        
{check}
  
  <Button icon="delete" mode="contained" onPress={()=> this.delete(this.state.data._id)} style={styles.button} color="#9134af" >
      
      Delete
    </Button>
  
 <View style={{padding:50}}></View>

  </ScrollView>
  </Surface>
        </PaperProvider>
      );
    }
  }
  export default withNavigation(App);
  const theme = {
    ...DefaultTheme,
    colors: {
      ...DefaultTheme.colors,
      primary: '#9134af',
      accent: '#ff5252',
    },
  };
  const styles = StyleSheet.create({
    bottom: {
      position: 'absolute',
      left: 0,
      right: 0,
      bottom: 0,
    },
    text:{
      width:'95%',
      borderRadius:0,
      backgroundColor:"white",
      marginBottom:10
    }
    ,
    item: {
      padding: 10,
      fontSize: 18,
      height: 44,
    },
    button:{
      color:'white',
      marginTop:10,
      width:'95%'
    },
    surface: {
      padding: 8,
      marginTop:10,
      paddingBottom:25,
      //height: 80,
      width: '100%',
      alignItems: 'center',
      justifyContent: 'center',
      elevation: 0,
    },
  });
