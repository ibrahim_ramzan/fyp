import React from 'react';
import { Platform, StatusBar, StyleSheet, View,Text,ScrollView,Dimensions ,FlatList } from 'react-native';
import { DefaultTheme, Provider as PaperProvider } from 'react-native-paper';
import { Appbar } from 'react-native-paper';
import { TextInput,Title,Button,Surface } from 'react-native-paper';
import { Avatar,Divider,Drawer } from 'react-native-paper';
import { List } from 'react-native-paper';
import { StackActions } from 'react-navigation';
import {Link} from './global';
export default class App extends React.Component {
  static navigationOptions = {
    header: null,
  };
  state = {
    isLoadingComplete: false,
    text:'',
    user:'',
    count : 1,
    data : [],
    active:'first'
  };

  render() {

    trySignup = () =>
    {
      console.log("opening signup");
      //this.props.signUp();
      this.props.navigation.navigate('Choose')
    }
    tryLogin  = () =>{
    
      }



    const { active } = this.state;
    const screenHeight = Dimensions.get('window').height;
    const popAction = StackActions.pop({
        n: 1,
      });
    let check;
    
    
    
    



      return (
        <PaperProvider theme={theme} style={{width:'80%',height:screenHeight}}>
        <Appbar.Header>
        <Avatar.Icon size={50} style={{marginRight:2}} icon="beach-access" />
        <Appbar.Content
          title="Save Tails" style={{marginLeft:2}}
        />
       
      </Appbar.Header>
        <Surface style={styles.surface}>
        <Title style={{color:"#9134af"}}>Notifications</Title>
        <ScrollView 
    behaviour = "height"
    keyboardVerticalOffset = {64}
    style= {{ marginTop: '5%',width:"100%",height:"100%",padding:5}}
  >
        
        <View style={styles.container}>
        
        <List.Section>
        <List.Subheader>New</List.Subheader>
        <List.Item
          title="ibrahim animal is in rescue state"
          left={() => <List.Icon icon="notifications" color="#9134af" />}
       />
        <List.Item
          title="Hamza , animal for rescue f8 markaz"
          left={() => <List.Icon color="#9134af" icon="notifications" />}
       />
     </List.Section>
       
    </View>
  
    <Button icon="backspace" mode="contained" style={styles.button} color="#9134af" onPress={() => this.props.navigation.navigate('WildLife')}>
      
      Back
    </Button>
  
 <View style={{padding:50}}></View>

  </ScrollView>
  </Surface>
        </PaperProvider>
      );
    }
  }
  const theme = {
    ...DefaultTheme,
    colors: {
      ...DefaultTheme.colors,
      primary: '#9134af',
      accent: '#ff5252',
    },
  };
  const styles = StyleSheet.create({
    bottom: {
      position: 'absolute',
      left: 0,
      right: 0,
      bottom: 0,
    },
    text:{
      width:'95%',
      borderRadius:0,
      backgroundColor:"white",
      marginBottom:10
    }
    ,
    item: {
      padding: 10,
      fontSize: 18,
      height: 44,
    },
    button:{
      color:'white',
      marginTop:10,
      width:'95%'
    },
    surface: {
      padding: 8,
      marginTop:10,
      paddingBottom:25,
      //height: 80,
      width: '100%',
      alignItems: 'center',
      justifyContent: 'center',
      elevation: 0,
    },
  });
