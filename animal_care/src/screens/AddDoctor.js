import React from 'react';
import { Platform, StatusBar, StyleSheet,Alert, View,Text,ScrollView,Dimensions } from 'react-native';
import { DefaultTheme, Provider as PaperProvider } from 'react-native-paper';
import { Appbar } from 'react-native-paper';
import { TextInput,Title,Button,Surface } from 'react-native-paper';
import { Avatar,Divider,Drawer } from 'react-native-paper';
import {Link} from './global';
import { withNavigation } from 'react-navigation';
 class App extends React.Component {
  static navigationOptions = {
    header: null,
  };
  state = {
    isLoadingComplete: false,
    name : '',
    email : '',
    city : '',
    phoneNo : '',
    address : '',
    bio:'',
    timing : '',
    ISO : '',
    website : '',
    text:'',
    username:'',
    degree:'',
    description:'',
    time:'',
    reg:'',
    password:'',
    active:'first',
    orgId:this.props.navigation.getParam('id',"0"),
  };

  trySignUp  = () =>{
    //alert(this.state.orgId)
    console.log("Create Account Function Called");
    fetch(`${Link}/organization/addDoctor`, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        name : this.state.name,
        userName: this.state.username,
        degree: this.state.degree,
        description:this.state.description,
        time: this.state.time,
        id:this.props.navigation.getParam('id',"0")
      }),
    })
      .then((response) => response.json())
      .then((responseJson) => {
          console.log(responseJson, 'res JSON');
          //alert(responseJson.message);
         
          if(responseJson.message == "Done")
          {
            alert(responseJson.message)
            alert("Doctor Added Successfully");
            this.props.navigation.navigate('OrgHome',{id:this.props.navigation.getParam('id',"0")});
          }
          else{
            alert(responseJson.message);
          }
      })
      .catch((error) => {
          console.error(error);
      });
      }

  render() {
    
    const { active } = this.state;
    const screenHeight = Dimensions.get('window').height;
      return (
        <PaperProvider theme={theme} style={{width:'80%',height:screenHeight}}>
        <Appbar.Header>
        <Avatar.Icon size={50} style={{marginRight:2}} icon="beach-access" />
        <Appbar.Content
          title="Save Tails" style={{marginLeft:2}}
        />
       
      </Appbar.Header>
        
      
        <Surface style={styles.surface}>
        <Title style={{color:"#9134af"}}>Add Doctor </Title>
        <ScrollView 
    behaviour = "height"
    keyboardVerticalOffset = {64}
    style= {{ marginTop: '5%',width:"100%",height:"100%",padding:5}}
  >
        
        <TextInput style={styles.text}
        label='Name'
        Type="outlined"
        value={this.state.name}
        onChangeText={name => this.setState({ name })}
      />
        <TextInput style={styles.text}
        label='Username'
        Type="outlined"
        value={this.state.username}
        onChangeText={username => this.setState({ username})}
      />
     
     <TextInput style={styles.text}
        label='Degree'
        Type="outlined"
        value={this.state.degree}
        onChangeText={degree => this.setState({ degree })}
      />
      <TextInput style={styles.text}
        label='Description'
        Type="outlined"
        value={this.state.description}
        onChangeText={description => this.setState({ description})}
      />
       <TextInput style={styles.text}
        label='Time'
        Type="outlined"
        value={this.state.time}
        onChangeText={time => this.setState({ time })}
      />
       
      
     


  <Button icon="add-circle-outline" mode="contained" style={styles.button} color="#9134af" onPress={this.trySignUp}>
      
    Add Doctor
  </Button>

  <Button icon="backspace" mode="contained" style={styles.button} color="#9134af" onPress={() => this.props.navigation.navigate('OrgHome',{id:this.props.navigation.getParam('id',"0")})}>
      
    Cancel
  </Button>
 <View style={{padding:50}}></View>

  </ScrollView>
  </Surface>
        </PaperProvider>
      );
    }
  }
  export default withNavigation(App);
  const theme = {
    ...DefaultTheme,
    colors: {
      ...DefaultTheme.colors,
      primary: '#9134af',
      accent: '#ff5252',
    },
  };
  const styles = StyleSheet.create({
    bottom: {
      position: 'absolute',
      left: 0,
      right: 0,
      bottom: 0,
    },
    text:{
      width:'95%',
      borderRadius:0,
      backgroundColor:"white",
      marginBottom:10
    }
    ,
    button:{
      color:'white',
      marginTop:10,
      width:'95%'
    },
    surface: {
      padding: 8,
      marginTop:10,
      paddingBottom:25,
      //height: 80,
      width: '100%',
      alignItems: 'center',
      justifyContent: 'center',
      elevation: 0,
    },
  });
