import React from "react";
import { View, Text,Button,Linking } from "react-native";
import { createSwitchNavigator, createStackNavigator, createAppContainer } from "react-navigation";


import firebase, { Firebase } from 'react-native-firebase';



import DetailsScreen from './src/screens/Auth/Auth';
import HomeScreen from './src/screens/HomeScreen';
import SignInScreen from './src/screens/Auth/SignInScreen';
import AuthLoadingScreen from './src/screens/Auth/AuthLoadingScreen';
import SharePlace from './src/screens/redux_check/SharePlace';
import FindPlace from './src/screens/redux_check/FindPlace';
import UserSignUp from './src/screens/UserSignUp';
import OrganizationSignUp from './src/screens/OrganizationSignup';
import ForgotPassword from './src/screens/ForgotPassword';
import ChangedPassword from './src/screens/ChangePassword';
import Dashboard from './src/screens/Dashboard';
//import SettingScreen from './src/screens/SettingsScreen';
//import login from './src/screens/Auth/log';
import { Provider as PaperProvider } from 'react-native-paper';
import ChooseSignUp from './src/screens/ChooseSignUp';
import configureStore from './src/store/configureStore';
import {Provider } from 'react-redux';
import Appointment from './src/screens/Appointment';
import Appointment2 from './src/screens/Appointment2';
import ListOrganizations from './src/screens/ListOrganizations';
import OrganizationView from './src/screens/OrganizationView';
import AnimalEmergency from './src/screens/AnimalEmergency';
import UserProfile from './src/screens/userProfile';
import OrgHome from './src/screens/OrgHome';
import RescueOrg from './src/screens/RescueOrg';
import WildLife from './src/screens/WildlifeOrg';
import NotificationsListOrg from './src/screens/NotificationsListOrg';
import Nearest from './src/screens/Nearest';
import Tinder from'./src/screens/Tinder';
import AddPost from './src/screens/AddPost';
import BuyAdopt from './src/screens/BuyAdopt';
import ChooseAdd from './src/screens/ChoosePost';
import AddSell from './src/screens/AddPost';
import AddTinder from './src/screens/AddTinder';
import AddDonate from './src/screens/AddDonate';
import AnimatedMarkers from './src/screens/redux_check/test'
import AddDoctor from './src/screens/AddDoctor';
import ListDoctors from './src/screens/ListDoctors';
import Notify from './src/screens/Notifications';
import Buy from './src/screens/Buy';
import Adopt from './src/screens/Adopt';
//import  { Notification } from 'react-native-firebase';

//import firebase from 'react-native-firebase';
//import type { Notification, NotificationOpen } from 'react-native-firebase';

import type { Notification, NotificationOpen } from 'react-native-firebase';



const store=configureStore();


const AppStack = createStackNavigator({Adopt:Adopt,Buy:Buy,Notify:Notify,ListDoctors:ListDoctors,AddDoctor:AddDoctor,AnimatedMarkers:AnimatedMarkers,AddDonate:AddDonate,AddTinder : AddTinder,AddSell:AddSell,ChooseAdd:ChooseAdd,BuyAdopt:BuyAdopt,Tinder:Tinder,NotificationsListOrg: NotificationsListOrg,WildLife:WildLife,RescueOrg:RescueOrg,UserProfile: UserProfile, AnimalEmergency: AnimalEmergency, OrganizationView: OrganizationView, ListOrganizations: ListOrganizations, Appointment:Appointment,Appointment2:Appointment2, Choose: ChooseSignUp,Home: Dashboard,Find: FindPlace, UserSignUp : UserSignUp,OrganizationSignUp : OrganizationSignUp ,
  ForgotPassword : ForgotPassword, ChangedPassword : ChangedPassword,Dashboard :Dashboard,OrgHome:OrgHome,AddPost:AddPost,Nearest:Nearest,SharePlace:SharePlace });
const AuthStack = createStackNavigator({ SignIn: SignInScreen });

let Navigation =  createAppContainer(createSwitchNavigator(
  {
    AuthLoading: AuthLoadingScreen,
    App: AppStack,
    Auth: AuthStack,
  },
  {
    initialRouteName: 'AuthLoading',
  }
))

export default class App extends React.Component {

  constructor(props: Props) { 
    super(props); 
    this.state = { 
      password: '', 
      isPasswordHidden: true, 
      toggleText: 'Show', 
      token : 'ib',
      fcmToken : ''
    }; 
} 

  

  async componentDidMount() {

    firebase.messaging().getToken()
    .then((token) => {
        console.warn('Device FCM Token: ', token);
       // alert(token)
        //Linking.openURL(`mailto:support@example.com?body=${token}`)
    });
    

     // Set the current token if it exists
     firebase.messaging().getToken().then((fcmToken) => {
      if (fcmToken) {
       InLocoEngage.setPushProvider({
         name: "google_fcm",
         token: fcmToken,
       });
     }
     this.state.fcmToken =fcmToken
   });

   this.onTokenRefreshListener = firebase.messaging().onTokenRefresh((fcmToken) => {
    this.setPushProvider(fcmToken)});

    
  

 
     

    const notificationOpen: NotificationOpen = await firebase.notifications().getInitialNotification();
    if (notificationOpen) {
        const action = notificationOpen.action;
        const notification: Notification = notificationOpen.notification;
        var seen = [];
        alert(JSON.stringify(notification.data, function(key, val) {
            if (val != null && typeof val == "object") {
                if (seen.indexOf(val) >= 0) {
                    return;
                }
                seen.push(val);
            }
            return val;
        }));
    } 
    const channel = new firebase.notifications.Android.Channel('test-channel', 'Test Channel', firebase.notifications.Android.Importance.Max)
            .setDescription('My apps test channel');
// Create the channel
    firebase.notifications().android.createChannel(channel);
    this.notificationDisplayedListener = firebase.notifications().onNotificationDisplayed((notification: Notification) => {
        // Process your notification as required
        // ANDROID: Remote notifications do not contain the channel ID. You will have to specify this manually if you'd like to re-display the notification.
    });
    this.notificationListener = firebase.notifications().onNotification((notification: Notification) => {
        // Process your notification as required
        notification
            .android.setChannelId('test-channel')
            .android.setSmallIcon('ic_launcher');
        firebase.notifications()
            .displayNotification(notification);
        
    });
    this.notificationOpenedListener = firebase.notifications().onNotificationOpened((notificationOpen: NotificationOpen) => {
        // Get the action triggered by the notification being opened
        const action = notificationOpen.action;
        // Get information about the notification that was opened
        const notification: Notification = notificationOpen.notification;
        var seen = [];
        alert(JSON.stringify(notification.data, function(key, val) {
            if (val != null && typeof val == "object") {
                if (seen.indexOf(val) >= 0) {
                    return;
                }
                seen.push(val);
            }
            return val;
        }) ,"Hello World");
        firebase.notifications().removeDeliveredNotification(notification.notificationId);
        
    });
}
componentWillUnmount() {
    this.notificationDisplayedListener();
    this.notificationListener();
    this.notificationOpenedListener();
}
 

  

 

  


  







  render() {
    
    console.log("hello ibrahim")
    console.log(this.state.fcmToken)

    return (
      <PaperProvider>
        
      
      <Provider store={store}>
        <Navigation />
      </Provider>
      </PaperProvider>
      
    );
  }
}
